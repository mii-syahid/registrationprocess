package com.myspace.customer_registration;

/**
 * This class was automatically generated by the data modeler tool.
 */

public class dataSendAccountTransaction implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	@org.kie.api.definition.type.Label(value = "transactionId")
	private java.lang.String transactionId;
	@org.kie.api.definition.type.Label(value = "transactionName")
	private java.lang.String transactionName;

	public dataSendAccountTransaction() {
	}

	public java.lang.String getTransactionId() {
		return this.transactionId;
	}

	public void setTransactionId(java.lang.String transactionId) {
		this.transactionId = transactionId;
	}

	public java.lang.String getTransactionName() {
		return this.transactionName;
	}

	public void setTransactionName(java.lang.String transactionName) {
		this.transactionName = transactionName;
	}

	public dataSendAccountTransaction(java.lang.String transactionId,
			java.lang.String transactionName) {
		this.transactionId = transactionId;
		this.transactionName = transactionName;
	}

}